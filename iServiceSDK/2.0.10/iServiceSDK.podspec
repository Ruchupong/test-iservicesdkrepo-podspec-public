Pod::Spec.new do |spec|
    spec.name                   = "iServiceSDK"
    spec.version                = "2.0.10"
    spec.summary                = "iService SDK"
    spec.description            = "iService SDK for iOS"
    spec.homepage               = "https://gitlab.com/Ruchupong/test-iservicesdkrepo-podspec-public"
    spec.license                = { :type => 'MIT', :file => 'LICENSE' }
    spec.author                 = { "Ruchupong Saengan" => "Ruchupong Saengan_EMAIL" }
    spec.source                 = { :git => "git@gitlab.com:Ruchupong/test-iservicesdkrepo-frameworks-public.git", :tag => spec.version.to_s }
    # spec.vendored_frameworks    = "App.xcframework","device_info.xcframework","flutter_inappwebview.xcframework","Flutter.xcframework","FlutterPluginRegistrant.xcframework","FMDB.xcframework","iservice_sdk.xcframework","OrderedSet.xcframework","path_provider.xcframework","sqflite.xcframework","url_launcher.xcframework","video_player.xcframework","wakelock.xcframework","webview_flutter.xcframework"
    spec.vendored_frameworks    = "*.xcframework"
    spec.platform               = :ios
    spec.swift_version          = "5"
    spec.ios.deployment_target  = '14.0'
  end
